"use strict";

module.exports = function(server, restify) {
  // Serves the app

  server.get(
    "*",
    restify.plugins.serveStatic({
      directory: "./public",
      default: "index.html"
    })
  );
};
