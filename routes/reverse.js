"use strict";

module.exports = function(server) {
  const history = [];

  const respondHistory = (res, next) => {
    res.send(buildResponse(""));
    next();
  };

  const addHistory = item => {
    const newObject = new Object();
    newObject.text = item;
    history.unshift(newObject);
    if (history.length > 5) {
      history.pop();
    }
  };

  const buildResponse = item => {
    const historyItems = new Object();
    historyItems.history = history;
    const result = new Object();
    result.result = item;

    return Object.assign({}, result, historyItems);
  };

  const reverseString = str => {
    return str.replace(/[a-zåäö0-9]+/gi, function(w) {
      return w
        .split("")
        .reverse()
        .join("");
    });
  };

  /**
   * Create
   */
  server.post("/reverse", (req, res, next) => {
    const data = req.params.payload;
    if (!data) {
      res.send(405, { error: "Ingen data att göra magi med!" });
      return;
    }
    if (data.length > 300) {
      res.send(405, { error: "Texten är tyvärr för lång. Max 300 tecken" });
      return;
    }

    try {
      const reversedString = reverseString(data);
      addHistory(reversedString);
      const response = buildResponse(reversedString);

      res.send(response);
      next();
    } catch (err) {
      res.send(500, { error: err });
    }
  });

  /**
   * List
   */
  server.get("/history", (req, res, next) => {
    respondHistory(res, next);
  });
};
