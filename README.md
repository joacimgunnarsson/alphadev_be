# alphadev

Site consists of three repositorys. One for frontend, one for backend and one for holding a http-server that delivers the frontend code for clients.

## Install

_Make sure that you install all repositorys in the same folder_

```bash
/alphadev_fe
/alphadev_be
/alphadevwebserver
```

For BE:
_run npm install_ to install dependencies
_npm run start_ to start project

## Testing

1. Start FE project
2. Start BE projcet
3. Run command in frontend folder _npm run cypress:open_ to open test dashboard or _npm run cypress:x_ to run test in CMD

## Deploy

Deploys are automatically triggerd on push to the master branch of _alphadev_be_ and _alphadev_feserver_. This is done with Bitbucket Pipeline. What basically happens is that after succcesfull build it pushes to the heroku master branch which reflects the hosting environment.

To deploy backend:

1. Merge changes to master branch in alphadev_be
2. Push

## Backend

Backend is built in Node with the Restify REST API framework. It has two routes _/reverse_ and _/history_. History is used to get latest history on app start and reverse is use to reverse a string.

## Production

Frontend https://alphadevwebserver.herokuapp.com/
Backend https://alphadevbe.herokuapp.com
