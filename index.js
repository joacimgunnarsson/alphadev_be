"use strict";
/**
 * Module Dependencies
 */
const restify = require("restify");

/**
 * Config
 */
const config = require("./config");

/**
 * Initialize Server
 */
const server = restify.createServer({
  name: config.name,
  version: config.version
});

/**
 * Bundled Plugins (http://restify.com/#bundled-plugins)
 */
server.use(restify.plugins.bodyParser({ mapParams: true }));
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser({ mapParams: true }));
server.use(restify.plugins.fullResponse());

const corsMiddleware = require("restify-cors-middleware");

const cors = corsMiddleware({
  origins: ["*"]
});

server.pre(cors.preflight);
server.use(cors.actual);

/**
 * Start Server & Require Route Files
 */
server.listen(config.port, () => {
  const opts = {
    promiseLibrary: global.Promise,
    auto_reconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
    autoIndex: true,
    useNewUrlParser: true,
    useCreateIndex: true
  };

  require("./routes/reverse")(server);

  console.log(`Server is listening on port ${config.port}`);
});
